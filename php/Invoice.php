<?php

//namespace;

/**
*  Bill Details per Invoice object
*
*/
class Invoice {

  public $invoiceItemsArray = [];
  public $invoiceNumber;
  public $issueDate;
  public $dueDate;
  public $payer;
  public $status;
  public $accessCode;
  public $fileName;


  public function __construct() {

  }

}
