<?php
class ObjectToJson implements JsonSerializable {
    public function __construct($object) {
        $this->object = $object;
    }

    public function jsonSerialize() {
        return $this->object;
    }
}

//$array = ['foo' => 'bar', 'quux' => 'baz'];
//echo json_encode(new ArrayValue($array), JSON_PRETTY_PRINT);
