function invoiceDetails() {
  $('.button').on('click', function(event){
    event.preventDefault();
    //console.log(event);
    var data1 = $('form').serialize();
    //`alert(data1);

    $.ajax({
      url: "php/InvoiceDetails.php",
      type: "POST",
      dataType: 'json',
      //cache: false,
      data:data1,
      success:function(data)
      {
        //console.log(data);
        if(data == '0'){
          alert('Email Address or Access Code not found');
        }
        else{
          $("#signin").hide();
          //console.log(data);

          var newButton = '<div class="control has-text-centered"><input class="button is-link" type="submit" value="New Invoice" onclick="location.reload(true);"></div>';

          var tableHeader = '<table class="table is-bordered"><thead><tr><th>service Time</th><th>quantity</th><th>Rate</th><th>Total</th><th>Notes</th></tr></thead><tbody>';

          $('#output').html(tableHeader);
          //console.dir(data.invoiceItemsArray);

          $.each(data.invoiceItemsArray, function(key, value)
          {
            console.log(key + ": " + value);
          });
          var tablebody = '';
          //$('#output').append('<tr><td>' + 'Hello' + '</td><td>' + 'World' + '</td>');
          for (var index in data.invoiceItemsArray)
          {
            tablebody += '<tr><td>'+data.invoiceItemsArray[index].serviceTime+'</td><td>'+data.invoiceItemsArray[index].quantity+'</td><td>'+data.invoiceItemsArray[index].rate+'</td><td>'+data.invoiceItemsArray[index].total+'</td><td>'+data.invoiceItemsArray[index].note+'</td></tr>';
          }
          $('#output').html(tableHeader += tablebody + newButton);

        } //else
      } //end success
    }); //end ajax
  }); //end click
}
