<?php
  include("DBConnect.php");
  include("Invoice.php");
  include("InvoiceItems.php");
  include("ObjectToJson.php");

  //print_r($dbconnect);

  //$yremail = $_POST['yremail'];
  //$yraccesscode = $_POST['yraccesscode'];
  //$chckbx  = $_POST['chckbx'];

  $yremail = "kchrisj@gmail.com";
  $yraccesscode = "100-AXI-001";
  //$chckbx  = "1";

  //echo "email address: " . $yremail;
  //echo "Access Code: " . $yraccesscode;
  //echo "Checked: " . $chckbx;
  //echo "hello!!";

  $invoice_query = "SELECT CI.number, CI.issued, CI.due, CI.payer, CI.status, CI.access_code, CI.filename FROM core_invoice AS CI, people AS P
  WHERE p.email = ? AND CI.access_code = ?";


  if($results = $dbconnect->prepare($invoice_query)){
    $results->bind_param("ss", $yremail, $yraccesscode);
    $results->execute();
    $results->store_result();

    /* bind result variables */
    $results->bind_result($invNumber, $issued, $due, $payer, $status, $accessCode, $filename);

    //var_dump($results);
  } //if

  $invoiceDetails = new Invoice();

  $rowCnt = $results->num_rows;

  if($rowCnt == 0){
    echo $rowCnt;
    die();
  }
  else {
    while($row = $results->fetch()) {
      $invoiceDetails->invoiceNumber = $invNumber;
      $invoiceDetails->issueDate = $issued;
      $invoiceDetails->dueDate = $due;
      $invoiceDetails->payer = $payer;
      $invoiceDetails->status = $status;
      $invoiceDetails->accessCode = $accessCode;
      $invoiceDetails->fileName = $filename;
    }

  }
  //var_dump($details);


  $items_query = "SELECT CII.invoice, CII.service_time, CII.quantity, CII.rate, CII.total, CII.note FROM core_invoice_item AS CII WHERE CII.invoice = ?";


  if($itemresults = $dbconnect->prepare($items_query)){
    $itemresults->bind_param("s", $invNumber);
    $itemresults->execute();
    $itemresults->store_result();

    /* bind result variables */
    $itemresults->bind_result($invoice, $serviceTime, $quantity, $rate, $total, $note);

    while($row = $itemresults->fetch()) {
      $invoiceItems = new InvoiceItems();
      //echo $serviceTime . "\r\n";
      //$invoiceItems->invoice = $invoice;
      $invoiceItems->quantity = $quantity;
      $invoiceItems->serviceTime = $serviceTime;
      $invoiceItems->rate = $rate;
      $invoiceItems->total = $total;
      array_push($invoiceDetails->invoiceItemsArray, $invoiceItems);
      //var_dump($invoiceItems);
  }
//  var_dump($invoiceDetails);



  //echo "----------------------------------- \r\n";
  //echo "hello";
  echo json_encode(new ObjectToJson($invoiceDetails), JSON_PRETTY_PRINT);

  //echo "\r\n";

  } //if
